package com.thinknsync.convertible;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public interface Convertible<T> {
    T fromJson(JSONObject jsonObject) throws JSONException;
    T fromMap(Map jsonObject);
    JSONObject toJson() throws JSONException;
    JSONObject toFullJson() throws JSONException;
    JSONObject withProperty(List<String> columns) throws JSONException;
    JSONObject withProperty(String[] columns) throws JSONException;
    JSONObject withoutProperty(List<String> columns) throws JSONException;
    JSONObject withoutProperty(String[] columns) throws JSONException;
}
