package com.thinknsync.convertible;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public abstract class BaseConvertible<T> implements Convertible<T> {

    public JSONObject withProperty(List<String> columnList) throws JSONException{
        return keepProperty(toJson(), columnList);
    }

    public JSONObject withProperty(String[] columnArray) throws JSONException{
        return keepProperty(toJson(), Arrays.asList(columnArray));
    }

    public JSONObject withoutProperty(List<String> properties) throws JSONException {
        return removeProperty(toJson(), properties);
    }

    public JSONObject withoutProperty(String[] properties) throws JSONException {
        return removeProperty(toJson(), Arrays.asList(properties));
    }

    public T fromMap(Map map) {
        try {
            return fromJson(new JSONObject(map));
        } catch (JSONException e){
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public JSONObject toFullJson() throws JSONException {
        return toJson();
    }

    public String toString(){
        try {
            return toJson().toString();
        } catch (JSONException e){
            e.printStackTrace();
        }
        return "";
    }

    private JSONObject keepProperty(JSONObject object, List<String> properties) {
        Iterator keys = object.keys();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            if (!properties.contains(key)) {
                keys.remove();
            }
        }
        return object;
    }

    private JSONObject removeProperty(JSONObject object, List<String> properties) {
        Iterator keys = object.keys();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            if (properties.contains(key)) {
                keys.remove();
            }
        }
        return object;
    }
}
